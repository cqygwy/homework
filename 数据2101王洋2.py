from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
driver=webdriver.Chrome()

#  编写一个用例，测试打开 https://ceshiren.com/
def test_open():
    driver.get("https://ceshiren.com/")
    sleep(3)

# 编写一个用例，测试点击操作
def test_click():
    driver.find_element(By.LINK_TEXT, "热门").click()
    sleep(3)

# 预期结果 : 找到 “热门” 这个词
def test_assert():
    assert driver.find_element(By.LINK_TEXT, "热门").text == "热门"
# 实际结果 : 测试通过
