import pytest
def sum(a, b):
    if a>99 or a<-99 :
        return False
    elif b>99 or b<-99 :
        return False
    else :
        return a+b

def setup_function():
    print("开始计算")
def teardown_function():
    print("结束计算")
def teardown():
    print("结束测试")

@pytest.mark.hebeu
def test_1_hebeu():
    assert sum(-100, 55)==45

@pytest.mark.hebeu
def test_2_hebeu():
    assert sum(2, 2)==4

@pytest.mark.hebeu
def test_3_hebeu():
    assert sum(99, 99)==198

@pytest.mark.hebeu
def test_4_hebeu():
    assert sum(55, 100)==155

@pytest.mark.hebeu
def test_5_hebeu():
    assert sum(-99, 99)==0

@pytest.mark.hebeu
def test_6_hebeu():
    assert sum(80, -99)==-19

def test_4():
    assert sum(55, 100)==155

def test_5():
    assert sum(-99, 99)==0

def test_6():
    assert sum(80, -99)==-19
